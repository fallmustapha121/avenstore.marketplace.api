package com.avenstore.marketplace.models;

public record ShopMetaData(String id,String label, String address) {
}
