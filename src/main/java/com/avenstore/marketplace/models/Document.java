package com.avenstore.marketplace.models;

import com.avenstore.marketplace.models.enums.DocumentType;

public record Document(DocumentType type,String url) {
}
