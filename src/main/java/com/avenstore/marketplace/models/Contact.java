package com.avenstore.marketplace.models;

public record Contact(String commercialPhone,String adminPhone,String commercialEmail, String adminEmail) {
}
