package com.avenstore.marketplace.models;

import com.avenstore.marketplace.models.enums.OfferType;
import com.avenstore.marketplace.models.enums.Size;

import java.util.List;
import java.util.Set;

public record Marketplace(
        String label,
        String address,
        Set<String> Categories,
        Set<Document> documents,
        Size size,
        OfferType offerType,
        String domainName,
        Contact contacts,
        String Country,
        String creationDate,
        String updateDate,
        List<ShopMetaData> shops
        ) {
}
