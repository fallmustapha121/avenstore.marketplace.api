package com.avenstore.marketplace.models.enums;

public enum OfferType {
    PRODUCTS, SERVICES, HYBRID
}
